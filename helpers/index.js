const fs = require('fs');
const path = require('path');

function checkStatic() {
  fs.stat('./static', (err) => {
    if(err) {
      fs.mkdir(path.join('./', 'static'), err => {
        if(err) {
          throw err;
        }
      })
    }
  })
}

module.exports = {
  checkStatic
}